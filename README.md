## Projet e-commerce
L'objectif de ce projet sera de réaliser un site de vente en ligne par groupes de 3-4 personnes en 3-4 semaines.
A votre équipe de trouver un sujet innovant et motivant (Que vous seriez fier(e) de présenter à vos futurs entretiens).
Le thème du site est "libre" (ça veut dire que si le thème est éthiquement limite, il sera refusé)
Les contraintes sont les suivantes :

## Conception

Réalisation de diagrammes de Use Case
Réalisation d'un diagramme de classe
Réalisation de maquettes (wireframe) mobile first


## Technique

Utilisation Symfony 4.2 avec la stack complète (Doctrine, sécurité, twig, etc.)
Application responsive (bootstrap)
Utilisation de git/gitlab pour le travail en groupe


## Organisation Agile

Faire au moins 4 User stories sur le modèle de celles proposées
4 sprints (un par semaine) avec les fonctionnalités à livrer

Au début de chaque sprint faire un mail à votre groupe de review indiquant les fonctionnalités priorisées (buy a feature) à livrer/presenter en fin de semaine
Faire un daily scrum de 15 minutes max pour échanger sur vos soucis
A la fin de chaque sprint, faire une review avec le groupe reviewer (20 minutes max) et une retrospective d'équipe. Garder une trace de la review dans une issue gitlab avec des cases à cocher.


Utiliser la partie Issues et les Boards du projet gitlab pour définir vos tâches et votre kanban

Vous réaliserez également un README avec vos différents diagrammes commentés et une petite présentation du projet.

## User Stories

1. En tant qu'utilisateur.ice, je veux pouvoir visualiser la liste des produits filtrés pour faciliter mon choix
2. En tant qu'utilisateur.ice, je veux pouvoir ajouter des produits à mon panier pour passer rapidement ma commande
3. En tant que visiteur.euse, je veux pouvoir créer mon compte pour accéder à des fonctionnalités supplémentaires du site
4. En tant qu'administrateur.ice, je veux pouvoir gérer les produits disponibles afin de modifier le catalogue
Rappel : vous pouvez découper les user story (culture du backlog)
5. En tant qu'utilisateur.ice je veux pouvoir afficher mon panier pour voir ou j'en suis.
6. En tant qu'utilisateur.ice je veux pouvoir laisser un commentaire pour donner mon avis sur les produits.
7. En tant qu'utilisateur.ice je veux pouvoir ajouter des produits aux favoris pour les retrouver facilement.
8. En tant qu'utilisateur.ice je veux pouvoir afficher mes dernieres commandes pour savoir ce que j'ai manger la derniere fois.

