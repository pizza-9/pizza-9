<?php

namespace App\Form;

use App\Entity\Pizza;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PizzaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('price')
            ->add('size', ChoiceType::class, [
                'choices'  => [
                    'Small' ,
                    'Medium' ,
                    'Grande' ,
                ],
                
                    ])
            ->add('image', FileType::class, [
                'required' => false,
                'label' => 'Image (JPEG file)',
                'attr' => ['placeholder' => 'Choose file'],
                'data_class' => null
            ])
            ->add('description')
            // ->add('user')
            // ->add('commands')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pizza::class,
        ]);
    }
}
