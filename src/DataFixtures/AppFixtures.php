<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Pizza;

class AppFixtures extends Fixture
{

   /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {

        $dona = new User();
        $dona->setAddress('villeurbanne');
        $dona->setEmail('dona@test.com');
        $dona->setName('Dona');
        $dona->setSurname('LaBoss');
        $dona->setPhoneNumber('0606060606');
        $dona->setPassword($this->encoder->encodePassword($dona, "1234"));
        $dona->setRole('ROLE_USER');
        
        $manager->persist($dona);

        $rapha = new User();
        $rapha->setAddress('croix-rousse');
        $rapha->setEmail('rapha@test.com');
        $rapha->setName('Rapha');
        $rapha->setSurname('LeBoss');
        $rapha->setPhoneNumber('0660606060');
        $rapha->setPassword($this->encoder->encodePassword($rapha, "1234"));
        $rapha->setRole('ROLE_ADMIN');

        $manager->persist($rapha);

        $margarita = new Pizza();
        $margarita->setName('Margarita');
        $margarita->setDescription('Ingrédients: Tomate, jambon, Mozzarella');
        $margarita->setPrice(7.50);
        $margarita->setSize('Normale');

        $manager->persist($margarita);

        $napolitaine = new Pizza();
        $napolitaine->setName('Napolitaine');
        $napolitaine->setDescription('Ingrédients: tomate, anchois, câpres, olives, tomates fraîches, huile d’olive, persillade, mozzarella');
        $napolitaine->setPrice(6.50);
        $napolitaine->setSize('Petite');

        $manager->persist($napolitaine);

        $orientale = new Pizza();
        $orientale->setName('Orientale');
        $orientale->setDescription('Ingrédients: tomate, merguez, oeuf, oignons, poivrons, mozzarella');
        $orientale->setPrice(6.50);
        $orientale->setSize('Petite');

        $manager->persist($orientale);

        $manager->flush();
    }
}
