<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Entity\Pizza;
use Doctrine\Common\Persistence\ObjectManager;

class FavoriteController extends AbstractController
{
    /**
     * @Route("/favorite/{pizza}", name="add_favorite")
     */
    public function index(Pizza $pizza, ObjectManager $manager)
    {
        $this->getUser()->addPizza($pizza);
    
        
        $manager->flush();

        return $this->redirectToRoute('one_pizza', array('pizza'=>$pizza->getId()));
    }

      /**
     * @Route("/favorite/", name="favorite")
     */
    public function favorite()
    {
        


        return $this->render('favorite/index.html.twig', [
            'pizzas'=>$this->getUser()->getPizzas()
        ]);
    }


}
