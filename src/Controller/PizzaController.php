<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Pizza;
use App\Form\PizzaType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\PizzaRepository;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\File;
use App\Repository\CommentsRepository;
use App\Form\CommentsType;
use App\Entity\Comments;

class PizzaController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('pizza/index.html.twig', [
            'controller_name' => 'PizzaController',
        ]);
    }

    // /**
    //  * @Route("/all-pizza", name="all_pizza")
    //  */
    // public function showAll(PizzaRepository $repo)
    // {
    //     $allpizza = $repo->findAll();

    //     return $this->render("pizza/all-pizza.html.twig", [
    //         "pizza" => $allpizza
    //     ]);
    // }

    /**
 * @Route ("/add-pizza" , name="add_pizza")
 */
    public function addPizza(Request $request, ObjectManager $manager, FileUploader $fileUploader)
    {

        $pizza = new Pizza();
        $oldImage='';
        $form = $this->createForm(PizzaType::class, $pizza);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $pizza->getImage();
            $fileName = $fileUploader->upload($file);

            $pizza->setImage($fileName);
            $manager->persist($pizza);
            $manager->flush();
            return $this->redirectToRoute('all_pizza',['filt'=>'asc']);
        }

        return $this->render("pizza/add-pizza.html.twig", [
            "form" => $form->createView(),
            'verb' => 'Add',
            'oldImage'=>$oldImage
        ]);
    }

    /**
     * @Route ("/pizza/{pizza}", name="one_pizza")
     */
    public function onePizza(Pizza $pizza, Request $request, ObjectManager $manager){

        $comments= new Comments();
        $form = $this->createForm(CommentsType::class, $comments);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $comments->setUser($this->getUser());
            $comments->setPizza($pizza);
            $manager->persist($comments);
            $manager->flush();
               
        }

        return $this->render("pizza/one-pizza.html.twig", [
            "form" => $form->createView(),
            "pizza" => $pizza,            
        ]);
    }


    /**
     * @Route("/remove-pizza/{pizza}", name="remove_pizza")
     */
    public function removePizza(Pizza $pizza, ObjectManager $manager)
    {

        $manager->remove($pizza);
        $manager->flush();

        return $this->redirectToRoute('add_pizza');
    }

    /**
     * @Route("/remove-comment/{id}", name="remove_comment")
     */
    public function removeComment(Comments $comments, ObjectManager $manager)
    {

        $manager->remove($comments);
        $manager->flush();

        return $this->redirectToRoute('one_pizza', array('pizza'=>$comments->getPizza()->getId()));

    }

    /**
     * @Route("/modify-pizza/{pizza}", name="modify_pizza")
     */
    public function modify(Request $request, ObjectManager $manager, Pizza $pizza = null, FileUploader $fileUploader)
    {

        $verb = 'Modify';
        $oldImage = '';

        if (!$pizza) {

            $pizza = new Pizza();
            $verb = 'Add';
        } else {
            $oldImage = $pizza->getImage();
            // $pizza->setImage( new File( $this->getParameter('images_directory') . '/'.$pizza->getImage()));
        }

        $form = $this->createForm(PizzaType::class, $pizza);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()->getImage()) {
                $file = $pizza->getImage();
                $fileName = $fileUploader->upload($file);

                $pizza->setImage($fileName);
            } else {
                $pizza->setImage($oldImage);
            }
            $manager->persist($pizza);
            $manager->flush();

            return $this->redirectToRoute('all_pizza');
        }

        return $this->render('pizza/add-pizza.html.twig', [
            'form' => $form->createView(),
            'verb' => $verb,
            'oldImage'=>$oldImage
        ]);
    }

    /**
     * @Route("/all-pizza/filter={filt}", name="all_pizza")
     */

    public function FilterPizza(string $filt,  PizzaRepository $repo) {
        
        if ($filt == 'asc') {

            return $this->render('pizza/all-pizza.html.twig', [    
                'pizza' => $repo->findBy([], ['price'=>'ASC'])
            ]);

        } else {
            return $this->render('pizza/all-pizza.html.twig', [    
                'pizza' => $repo->findBy([], ['price'=>'DESC'])
            ]);
            
        }
        

    }
}
