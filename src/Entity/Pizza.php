<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PizzaRepository")
 */
class Pizza
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $size;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="pizzas")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comments", mappedBy="pizza")
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Commands", mappedBy="pizza")
     */
    private $commands;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\CommandLine", mappedBy="pizza", cascade={"persist", "remove"})
     */
    private $commandLine;

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->commands = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
        }

        return $this;
    }

    /**
     * @return Collection|Comments[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comments $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setPizza($this);
        }

        return $this;
    }

    public function removeComment(Comments $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getPizza() === $this) {
                $comment->setPizza(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commands[]
     */
    public function getCommands(): Collection
    {
        return $this->commands;
    }

    public function addCommand(Commands $command): self
    {
        if (!$this->commands->contains($command)) {
            $this->commands[] = $command;
            $command->addPizza($this);
        }

        return $this;
    }

    public function removeCommand(Commands $command): self
    {
        if ($this->commands->contains($command)) {
            $this->commands->removeElement($command);
            $command->removePizza($this);
        }

        return $this;
    }

    public function getCommandLine(): ?CommandLine
    {
        return $this->commandLine;
    }

    public function setCommandLine(?CommandLine $commandLine): self
    {
        $this->commandLine = $commandLine;

        // set (or unset) the owning side of the relation if necessary
        $newPizza = $commandLine === null ? null : $this;
        if ($newPizza !== $commandLine->getPizza()) {
            $commandLine->setPizza($newPizza);
        }

        return $this;
    }

    
}
