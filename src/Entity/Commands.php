<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandsRepository")
 */
class Commands
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commands")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Pizza", inversedBy="commands")
     */
    private $pizza;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\CommandLine", mappedBy="command", cascade={"persist", "remove"})
     */
    private $commandLine;

    public function __construct()
    {
        $this->pizza = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Pizza[]
     */
    public function getPizza(): Collection
    {
        return $this->pizza;
    }

    public function addPizza(Pizza $pizza): self
    {
        if (!$this->pizza->contains($pizza)) {
            $this->pizza[] = $pizza;
        }

        return $this;
    }

    public function removePizza(Pizza $pizza): self
    {
        if ($this->pizza->contains($pizza)) {
            $this->pizza->removeElement($pizza);
        }

        return $this;
    }

    public function getCommandLine(): ?CommandLine
    {
        return $this->commandLine;
    }

    public function setCommandLine(?CommandLine $commandLine): self
    {
        $this->commandLine = $commandLine;

        // set (or unset) the owning side of the relation if necessary
        $newCommand = $commandLine === null ? null : $this;
        if ($newCommand !== $commandLine->getCommand()) {
            $commandLine->setCommand($newCommand);
        }

        return $this;
    }
}
